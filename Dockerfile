FROM nodered/node-red:4.0.1-minimal

COPY package.json /data
COPY package-lock.json /data
COPY static /data/static
COPY flows.json /data
COPY settings.js /data

WORKDIR /data

RUN npm ci

WORKDIR /usr/src/node-red
