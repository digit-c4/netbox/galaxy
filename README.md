# C4 Gateway Galaxy

## Install the service on your lab

For installing the C4 Gateway Galaxy service on your lab, the first thing you
need to do is to clone the c4 gateway galaxy repository on your lab or local
machine.

```bash
git clone c4_gateway_galaxy_repo.git
```

After that, you will access the folder that has been created with the name
*c4-gateway-galaxy* with the command:

```bash
cd c4-gateway-galaxy
```

Once you are in, you will make sure you have Docker installed at least in
version 27.1.1 and you will run the following set of commands to avoid issues
when the C4 Gateway Galaxy container starts:

```bash
docker run --rm -it --entrypoint /bin/sh nodered/node-red -c "id"
sudo chown -R 1000:1000 .
sudo chmod -R 755 .
```

After ran those commands, you will execute the following :

```bash
env TAG=testing-v4 docker compose up -d
```

After that, you will check if the service is up and running by checking on if
there is a line like this when you run *docker ps -a*:

```
1dbb13ef71cd   nodered/node-red                                         "./entrypoint.sh"        17 hours ago   Up 17 hours (healthy)   0.0.0.0:1882->1880/tcp, :::1882->1880/tcp   c4-gateway-galaxy-c4-gtw-gxy-1
```

If so, and it is healthy, you will jump to internet and write *your_hostname:1882* which will open the C4 Gateway Galaxy service for you.

## Use the service

For using the service you will just need to go to a Netbox where this service is
installed on and search for the following: "hostname:1882"

There, you will click the hyperlink at the bottom saying *Service usage and console.*

Once you are in, you will see a smithy file that allows you to interact with any
of the given operations.

Take into account the following considerations:

**Get IS Bubble operation:**

In the latest update, we added query parameters (username and password). Now, to get the location of the IS bubble, you need to specify a user and its password as it appears on the Contact data model of the Organization section on Netbox. In addition to that, the user must have assigned the same tenant as the IS Bubble.

Possible HTTP response codes:

- 200 OK --> Everything went well, gateway galaxy url is retrieved.
- 401 Unauthorized --> If the user exists but the tenant the user is assigned to, does not match with the tenant of the IS bubble.
- 404 Not Found --> If the user has not been found / The customer reference has not been found on the existing bubbles / IS Bubble exists but does not have a URL to retrieve

**Create User operation:**

We added this operation in our latest update, so people with the admin token can create users under the Contact data model that will be used to retrieve the IS Bubble locations

Possible HTTP response codes:

- 204 Created --> The user is created sucessfully
- 401 Unauthorized --> Admin token introduced does not match with the NETBOX_API_TOKEN environment variables value
- 404 Not Found --> If the tenant we assign to the user has not been found
- 400 Bad Request --> If any of the above situations do not happen

## Run the tests

Tests are run with [Robot Framework](https://robotframework.org/)

First we need to install Robot Framework in a venv. For that in the root of the project do:

```bash
python3 -m venv venv
source venv/bin/activate
pip install robotframework robotframework-requests
```

Run the testing environment:

```bash
env TAG=testing-v4 docker-compose up -d
```

Then execute tests with dev environment started:

```bash
robot tests/
```
