C4 Gateway Galaxy
=================

Service definition
------------------

`OpenAPI Format <_static/openapi.html>`_

`Service Installation <_static/installation.html>`_

`Service Usage <_static/usage.html>`_

`Sequence Diagram <_static/sequence_diagram.html>`_
