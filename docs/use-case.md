# C4 Gateway Galaxy Use Cases

## Get C4 Gateway Bubble Gateway Location

The first use case of the C4 Gateway Galaxy service is to provide DIGIT C4 service customers with the information they need to consume the DIGIT C4 business primitives exposed by the C4 Gateway Bubble service.

Indeed, each customer has different applications encapsulated in an Information System (IS). For each IS of these customers, DIGIT C4 creates what we call a bubble containing the services provided by DIGIT C4. These services will then be consumed via a C4 Gateway Bubble service present in each bubble.
We therefore need to solve the problem of providing DIGIT C4 customers with the location of the C4 Gateway Bubble associated with a given IS.

That's why we decided to expose a centralized operation in C4 Gateway Galaxy that allows each customer with IS managed by DIGIT C4 to easily find the management endpoint.

In the context of the integration with DIGIT.B4 hosting services, B4 is acting as an intermediate provider on behalf of the final customer. In this context, they will provide us with a unique ID for each IS they will host and that will be linked to a dedicated bubble.  It is based on this unique ID that they must be able to get the information on the Bubble the interact with for the specific IS.

While B4 will have to garantee the uniqueness of this ID, considering that B4 will not be our only customer, we might need to namespace this ID based on the entity generating it.

![Sequence Diagram that describe activities in order to consume DIGIT C4 Service provided by a Bubble](sequencediagram_find_my_bubble.png)

### Actors
* **IS Owner**: Is reponsible to publish an IS that requires  "bubble" services
* **Hosting provider**: Intermediate entity providing on one side hosting services to an IS owner, and on the other automations to interact with underlying service providers
* **API Gateway Galaxy**: provide to Customers location of C4 Gateway Bubble that manage given IS
* **Netbox Galaxy**: index relations between Bubbles and Customer's IS
* **API Gateway Bubble**: expose DIGIT C4 business primitives
* **Netbox Bubble**: store configurations of DIGIT C4 services and trigger the jobs execution.

## Use Case

![Customer get C4 Gateway Bubble Location by providing a IS identifier](./assets/Get%20C4%20Gateway%20Bubble%20Location.png)

Actor by providing his IS identifier get in return the location of the C4 Gateway Bubble that manage this IS.

