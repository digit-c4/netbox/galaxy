$version: "2.0"

namespace eu.europa.ec.snet

use aws.protocols#restJson1
use smithy.api#httpApiKeyAuth

@title("Baby Galaxy")
@restJson1
service Galaxy {
    version: "1.0.0"
    resources: [
        Owner,
        Provider,
        Bubble
    ]
}

@title("C4 Gateway Galaxy Public")
@httpApiKeyAuth(name: "adminToken", in: "header")
@httpBasicAuth
@restJson1
@auth([httpApiKeyAuth, httpBasicAuth])
service C4GatewayGalaxyPublic {
    version: "2.2.0"
    operations: [
        GetIsBubbleByCustomerReference,
        CreateUser,
        UpdateUser,
        GetMetrics
    ]
}
