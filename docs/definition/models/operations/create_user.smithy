$version: "2.0"

namespace eu.europa.ec.snet

@tags(["Customer"])
@http(method: "POST", uri: "/api/{tenant}/user", code: 201)
@documentation("Create a user in Netbox Galaxy to be used to connect to bubbles")
@examples(
    [
        {
            title: "Create User",
            documentation: "Creates a user",
            input: {
                username: "parervi",
                password: "parervi123",
                tenant: "RPS"
            }
        },
        {
            title: "User Already Existing",
            documentation: "A user is already existing",
            input: {
                username: "parervi",
                password: "parervi123",
                tenant: "RPS"
            },
            error: {
                shapeId: CreateUserAlreadyFound,
                content: {
                    code: 409,
                    message: "User `parervi` is already existing"
                }
            }
        },
        {
            title: "Token Not Authorized",
            documentation: "A wrong token is being used to create a user",
            input: {
                username: "parervi",
                password: "parervi123",
                tenant: "RPS"
            },
            error: {
                shapeId: TokenNoAuthorized,
                content: {
                    code: 401,
                    message: "Token `thistokenisnot4u` is not authorized to create a user."
                }
            }
        }
    ]
)


@auth([httpApiKeyAuth])
operation CreateUser {
    input: CreateUserInput,
    errors: [
        CreateUserAlreadyFound,
        TokenNoAuthorized
    ]
}

structure CreateUserInput {

    @required
    @httpLabel
    tenant: String

    username: String,
    password: String
}

@error("client")
@httpError(409)
structure CreateUserAlreadyFound {
    code: Integer,
    message: String
}
@error("client")
@httpError(401)
structure TokenNoAuthorized {
    code: Integer,
    message: String
}
