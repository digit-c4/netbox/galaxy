$version: "2.0"

namespace eu.europa.ec.snet

@tags(["Customer"])
@readonly
@http(method: "GET", uri: "/is/{customer_reference}")
@documentation("Get IS Bubble information by its unique public name")
@examples(
    [
        {
            title: "Get IS Bubble",
            documentation: "Ask for a IS Bubble details",
            input: {
                customer_reference: "myis",
                queryParams:{
                    "username": "John Doe",
                    "password": "thispasswordisnot4u"
                }
            },
            output: {
                gateway_base_url: "http://gateway.myis.nms.tech.ec.europa.eu"
            }
        },
        {
            title: "IS Bubble not found",
            documentation: "Ask for an IS Bubble that doesn't exist",
            input: {
                customer_reference: "thisdoesnotexists"
            },
            error: {
                shapeId: IsBubbleNotFound,
                content: {
                    code: 404,
                    message: "IS `thisdoesnotexists` not found"
                }
            }
        },
        {
            title: "Customer reference without Gateway base URL",
            documentation: "Ask for an IS Bubble that exists but that does not have the Gateway base url informed",
            input: {
                customer_reference: "dev"
            },
            error: {
                shapeId: IsBubbleNotFound,
                content: {
                    code: 404,
                    message: "Bubble with customer reference `dev` does not have a galaxy gateway url"
                }
            }
        },
        {
            title: "User not authorized",
            documentation: "The user is not authorized to access the resource",
            input: {
                customer_reference: "RPS"
            },
            error: {
                shapeId: NoAuthorized,
                content: {
                    code: 401,
                    message: "User `John Doe` is not authorized to get this resource."
                }
            }
        }
    ]
)
@auth([httpBasicAuth])
operation GetIsBubbleByCustomerReference {
    input: GetIsBubbleByCustomerReferenceInput,
    output: GetIsBubbleOutput,
    errors: [
        IsBubbleNotFound,
        NoAuthorized
    ]
}

structure GetIsBubbleByCustomerReferenceInput {
    @required
    @httpLabel
    customer_reference: String

}
map QueryParams {
    key: String,
    value: String
}

structure GetIsBubbleOutput {
    gateway_base_url: String
}

@error("client")
@httpError(404)
structure IsBubbleNotFound {
    code: Integer,
    message: String
}

@error("client")
@httpError(404)
structure GatewayURLNotFound {
    code: Integer,
    message: String
}

@error("client")
@httpError(401)
structure NoAuthorized {
    code: Integer,
    message: String
}