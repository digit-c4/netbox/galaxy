$version: "2.0"

namespace eu.europa.ec.snet

@tags(["Customer"])
@http(method: "PUT", uri: "/api/user", code: 201)
@documentation("Create a user in Netbox Galaxy to be used to connect to bubbles")
@examples(
    [
        {
            title: "Update User",
            documentation: "Updates an existing user",
            input: {
                tenants: ["RPS", "DEV"],
                username: "parervi"
                
            }
        },
        {
            title: "Tenant already assigned",
            documentation: "A tenant is already assigned to the user",
            input: {
                tenants: ["ISP"],
                username: "parervi"
            },
            error: {
                shapeId: TenantAlreadyAssigned,
                content: {
                    code: 409,
                    message: "User `parervi` already has `ISP` tenant assigned"
                }
            }
        },
        {
            title: "Tenant not existing",
            documentation: "A tenant does not exist",
            input: {
                tenants: ["XTS"],
                username: "parervi"
            },
            error: {
                shapeId: TenantNotFound,
                content: {
                    code: 404,
                    message: "Tenant `XTS` does not exist"
                }
            }
        },
        {
            title: "Token Not Authorized",
            documentation: "A wrong token is being used to create a user",
            input: {
                tenants: ["XTS"],
                username: "parervi"
            },
            error: {
                shapeId: TokenNoAuthorized,
                content: {
                    code: 401,
                    message: "Token `thistokenisnot4u` is not authorized to create a user."
                }
            }
        }
    ]
)


@auth([httpApiKeyAuth])
operation UpdateUser {
    input: UpdateUserInput,
    errors: [
        TenantAlreadyAssigned,
        TenantNotFound,
        TokenNoAuthorized
    ]
}

structure UpdateUserInput {
    tenants: TenantsList,
    username: String
}

list TenantsList {
    member: String
}

@error("client")
@httpError(409)
structure TenantAlreadyAssigned {
    code: Integer,
    message: String
}

@error("client")
@httpError(404)
structure TenantNotFound {
    code: Integer,
    message: String
}
@error("client")
@httpError(401)
structure TokenNoAuthorized {
    code: Integer,
    message: String
}
