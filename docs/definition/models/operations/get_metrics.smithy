$version: "2.0"

namespace eu.europa.ec.snet

@readonly
@tags(["Operation"])
@http(method: "GET", uri: "/api/metrics")
@documentation("Get the Prometheus service metrics.")
@examples(
    [
        {
            title: "Get Prometheus service metrics"
            documentation: "Ask for the Prometheus service metrics."
            output: {
                contentType: "text/plain"
                content: "# HELP failed_is_retrieve_total Number of IS calls that retrieved a is incorrectly
# TYPE failed_is_retrieve_total counter
failed_is_retrieve_total 0
# HELP success_is_retrieve_total Number of IS calls that retrieved a is properly
# TYPE success_is_retrieve_total counter
success_is_retrieve_total 4

# HELP existing_users_total Number of calls that retrieved an existing user
# TYPE existing_users_total counter
existing_users_total 1

# HELP success_created_user_total Number of calls that created a user
# TYPE success_created_user_total counter
success_created_user_total 6

# HELP tenant_not_found_total Number of calls that did not find a tenant
# TYPE tenant_not_found_total counter
tenant_not_found_total 2

# HELP unathorized_requests_total Number of IS calls that are unauthorized
# TYPE unathorized_requests_total counter
unathorized_requests_total 4

# HELP success_edited_user_total Number of calls that edited a user
# TYPE success_edited_user_total counter
success_edited_user_total 7"
            }
        }
    ]
)
operation GetMetrics {
    output := {
        @required
        @httpHeader("Content-Type")
        @documentation("Content-Type: text/plain")
        contentType: String = "text/plain"

        @httpPayload
        content: String
    }
}