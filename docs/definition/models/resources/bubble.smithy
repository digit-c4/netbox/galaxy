$version: "2.0"

namespace eu.europa.ec.snet

resource Bubble {
    identifiers: {bubbleId: String}
    properties: {netboxUrl: String, subnetId: String, usages: UsageList}
    create: CreateBubble
    read: GetBubble
    list: ListBubbles
    delete: DeleteBubble
}

list UsageList {
    member: Usage
}

string Usage

@error("client")
@httpError(404)
structure NotFound {
    @required
    message: String
}

@http(method: "POST", uri: "/bubbles")
@documentation("create Bubble")
@tags(["Bubble"])
operation CreateBubble {
    input := for Bubble {
        @required
        $subnetId

        $usages
    }
    output := for Bubble {
        @required
        $bubbleId

        @required
        $netboxUrl
    }
}

@readonly
@http(method: "GET", uri: "/bubbles/{bubbleId}")
@documentation("get Bubble with Id")
@tags(["Bubble"])
operation GetBubble {
    input := for Bubble {
        @required
        @httpLabel
        $bubbleId
    }
    output := for Bubble {
        @required
        $bubbleId

        @required
        $netboxUrl

        @required
        $subnetId

        $usages
    }
}

@readonly
@http(method: "GET", uri: "/bubbles")
@tags(["Bubble"])
@documentation("Get all Bubbles")
operation ListBubbles {
    input := {
        @httpQuery("ownerId")
        ownerId: String

        @httpQuery("providerId")
        providerId: String
    }
    output := {
        @required
        bubbles: BubbleList
    }
}

@tags(["Bubble"])
@documentation("Delete Bubble with Id")
@idempotent
@http(method: "DELETE", uri: "/bubbles/{bubbleId}")
operation DeleteBubble{
    input := for Bubble {
        @required
        @httpLabel
        $bubbleId
    }
    errors: [NotFound]
}



list BubbleList {
    member: BubbleOutput
}

structure BubbleOutput for Bubble {
    @required
    $bubbleId

    @required
    $subnetId

    @required
    $netboxUrl

    $usages
}
