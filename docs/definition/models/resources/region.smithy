$version: "2.0"

namespace eu.europa.ec.snet

resource Region {
    identifiers: {regionId: String, providerId: String}
    properties: {name: String}
    create: CreateRegion
    read: GetRegion
    list: ListRegion
}

@http(method: "POST", uri: "/providers/{providerId}/regions")
@tags(["Region"])
operation CreateRegion {
    input := for Region {
        @required
        @httpLabel
        $providerId

        @required
        $name
    }
    output := for Region {
        @required
        $regionId
    }
}

@readonly
@http(method: "GET", uri: "/providers/{providerId}/regions/{regionId}")
@tags(["Region"])
operation GetRegion {
    input := for Region {
        @required
        @httpLabel
        $providerId

        @required
        @httpLabel
        $regionId
    }
    output := for Region {
        @required
        $providerId

        @required
        $regionId

        @required
        $name
    }
}

@readonly
@http(method: "GET", uri: "/providers/{providerId}/regions")
@tags(["Region"])
operation ListRegion {
    input := for Region {
        @required
        @httpLabel
        $providerId
    }
    output := {
        regions: RegionList
    }
}

list RegionList {
    member: RegionOutput
}

structure RegionOutput for Region {
    @required
    $providerId

    @required
    $regionId

    @required
    $name
}
