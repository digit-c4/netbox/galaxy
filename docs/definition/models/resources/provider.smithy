$version: "2.0"

namespace eu.europa.ec.snet

resource Provider {
    identifiers: {providerId: String}
    properties: {name: String}
    create: CreateProvider
    read: GetProvider
    list: ListProvider
    resources: [
        Subnet,
        Region
    ]
}

@http(method: "POST", uri: "/providers")
@tags(["Provider"])
operation CreateProvider {
    input := for Provider {
        @required
        $name
    }
    output := for Provider {
        @required
        $providerId
    }
}

@readonly
@http(method: "GET", uri: "/providers/{providerId}")
@tags(["Provider"])
operation GetProvider {
    input := for Provider {
        @required
        @httpLabel
        $providerId
    }
    output := for Provider {
        @required
        $providerId

        @required
        $name
    }
}

@readonly
@http(method: "GET", uri: "/providers")
@tags(["Provider"])
operation ListProvider {
    input := for Subnet {
        @httpQuery("subnetId")
        $subnetId
    }
    output := {
        @required
        providers: ProviderList
    }
}

list ProviderList {
    member: ProviderOutput
}

structure ProviderOutput for Provider {
    @required
    $providerId

    @required
    $name
}
