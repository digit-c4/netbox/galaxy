$version: "2.0"

namespace eu.europa.ec.snet

resource Subnet {
    identifiers: {subnetId: String, providerId: String}
    properties: {ownerId: String, cidr: String, regionId: String, ipAddresses: IpAddressList, domainName: String}
    create: CreateSubnet
    read: GetSubnet
    list: ListSubnet
}

list IpAddressList {
    member: IpAddress
}

structure IpAddress {
    @pattern("^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$")
    @length(min: 7, max: 16)
    @required
    ip: String

    service: Service
}

structure Service {
    @required
    fqdn: String

    @required
    tcpPort: Integer

    @required
    name: String

    @required
    @documentation("1.0.0")
    @pattern("^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?$")
    version: String
}

@http(method: "POST", uri: "/providers/{providerId}/subnets")
@tags(["Subnet"])
operation CreateSubnet {
    input := for Subnet {
        @required
        @httpLabel
        $providerId

        @required
        $regionId

        @required
        $ownerId

        @required
        $cidr

        @required
        $ipAddresses

        @required
        $domainName
    }
    output := for Subnet {
        @required
        $subnetId
    }
}

@readonly
@http(method: "GET", uri: "/providers/{providerId}/subnets/{subnetId}")
@tags(["Subnet"])
operation GetSubnet {
    input := for Subnet {
        @required
        @httpLabel
        $providerId

        @required
        @httpLabel
        $subnetId
    }
    output := for Subnet {
        @required
        $subnetId

        @required
        $providerId

        @required
        $regionId

        @required
        $ownerId

        @required
        $cidr

        @required
        $ipAddresses

        @required
        $domainName
    }
}

@readonly
@http(method: "GET", uri: "/providers/{providerId}/subnets")
@tags(["Subnet"])
operation ListSubnet {
    input := for Subnet {
        @required
        @httpLabel
        $providerId
    }
    output := {
        @required
        subnets: SubnetList
    }
}

list SubnetList {
    member: SubnetOutput
}

structure SubnetOutput for Subnet {
    @required
    $subnetId

    @required
    $providerId

    @required
    $regionId

    @required
    $ownerId

    @required
    $cidr

    @required
    $ipAddresses

    @required
    $domainName
}
