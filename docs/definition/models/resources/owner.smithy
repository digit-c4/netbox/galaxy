$version: "2.0"

namespace eu.europa.ec.snet

resource Owner {
    identifiers: {ownerId: String}
    properties: {name: String}
    create: CreateOwner
    read: GetOwner
    list: ListOwners
    update: UpdateOwner
}

@http(method: "POST", uri: "/owners")
@tags(["Owner"])
operation CreateOwner {
    input := for Owner {
        @required
        $name
    }
    output: OwnerOutput
}

@readonly
@http(method: "GET", uri: "/owners/{ownerId}")
@tags(["Owner"])
operation GetOwner {
    input := for Owner {
        @required
        @httpLabel
        $ownerId
    }
    output: OwnerOutput
}

@readonly
@http(method: "GET", uri: "/owners")
@tags(["Owner"])
operation ListOwners {
    input := for Bubble {
        @httpQuery("bubbleId")
        $bubbleId
    }
    output := {
        @required
        owners: OwnerList
    }
}

list OwnerList {
    member: OwnerOutput
}

structure OwnerOutput for Owner {
    @required
    $ownerId

    @required
    $name
}

@http(method: "POST", uri: "/owners/{ownerId}")
@tags(["Owner"])
operation UpdateOwner {
    input := for Owner {
        @required
        @httpLabel
        $ownerId

        $name
    }
    output := for Owner {
        @required
        $ownerId

        @required
        $name
    }
}
