*** Settings ***
Test Tags  Docs
Library  RequestsLibrary
LIbrary  Process

*** Variables ***

${BASE_URL}  http://localhost:1882
${USER_API}  admin
${PASSWORD_API}  password
${PROJECT_PATH}  project/

*** Test Cases ***

Test tag versions, written down on docs/.
    ${auth}=  Evaluate  ("${USER_API}", "${PASSWORD_API}")
    ${response_home}=  GET  ${BASE_URL}  auth=${auth}  expected_status=200
    ${response}=  GET  ${BASE_URL}/docs  auth=${auth}  expected_status=200

    # first line with tag version
    ${res0}=    Run Process    echo '${response.text}'|grep 'Gateway Galaxy'| awk '$0 {print $7}'| sed 's/.$//' |head -n 1    shell=true
    # second line with tag version
    ${res1}=    Run Process    echo '${response.text}'|grep 'Gateway Galaxy'| awk '$0 {print $7}'| sed 's/..title.$//' | tail -n 1    shell=true
    # home_page tag version
    ${res2}=    Run Process    echo '${response_home.text}'|grep 'Gateway Galaxy'| awk '$0 {print $5}'| sed 's/..title.$//' | head -n 1    shell=true
    # second line home_page tag version
    ${res3}=    Run Process    echo '${response_home.text}'|grep 'Gateway Galaxy'| awk '$0 {print $5}'| sed 's/..h1.$//' | tail -n 1    shell=true
    Log to console    \n
    Log to console    \n
    Log to console    \npath: static/docs/index.html
    Log to console    ${res0.stdout}
    Log to console    path: static/docs/index.html
    Log to console    ${res1.stdout}
    Log to console    path: static/index.html
    Log to console    ${res2.stdout}
    Log to console    path: static/index.html
    Log to console    ${res3.stdout}

    Should Be Equal    ${res0.stdout}  ${res1.stdout}
    Should Be Equal    ${res0.stdout}  ${res2.stdout}
    Should Be Equal    ${res0.stdout}  ${res3.stdout}

Test versions on different files.
    ${auth}=  Evaluate  ("${USER_API}", "${PASSWORD_API}")
    ${response}=  GET  ${BASE_URL}/docs  auth=${auth}  expected_status=200
    # first line with tag version
    ${res0}=    Run Process    echo '${response.text}'|grep 'Gateway Galaxy'| awk '$0 {print $7}'| sed 's/.$//' |sed 's/^.//' |head -n 1    shell=true
    Log to console    ${res0.stdout}

    # if we are already into project folder, choose right path to "./"
    ${res4}=  Run Process  ls ${PROJECT_PATH} |wc -l  shell==true
    IF  '${res4.stdout}' == '0'
      ${PROJECT_PATH}  Set Variable  ./
      Log to console    ${res4.stdout}
      Log to console    ${PROJECT_PATH}
    END

    # text on file
    ${res1}=    Run Process    cat ${PROJECT_PATH}docs/definition/models/service.smithy |grep Gateway -A 1    shell=true
    ${res2}=    Run Process    cat ${PROJECT_PATH}package.json |grep gateway -A 1   shell=true

    Log to console    ${PROJECT_PATH}docs/definition/models/service.smithy
    Should Contain    ${res1.stdout}  ${res0.stdout}

    Log to console    ${PROJECT_PATH}package.json
    Should Contain    ${res2.stdout}  ${res0.stdout}
