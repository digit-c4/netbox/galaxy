*** Settings ***
Test Tags  Docs
Library  RequestsLibrary

*** Variables ***
${BASE_URL}       http://localhost:1882
${ADMIN_TOKEN}    this1tokenshouldbeusedfortestingpurposes
${TENANT}    DEV
${ENDPOINT}      /api/${TENANT}/user
${USERMAME}    robot_emptyurl_test
${PASSWORD}    robot_emptyurl_test

*** Test Cases ***

Create user for test
    # Define Authentication
    ${auth}=  Evaluate  ("${USERMAME}", "${PASSWORD}")
    # Define the headers
    ${headers}=    Create Dictionary    accept=*/*    adminToken=${ADMIN_TOKEN}    content-type=application/json
    # Define the payload
    ${payload}=    Create Dictionary    username=${USERMAME}   password=${PASSWORD}
    # Perform the POST request
    ${response}=    Post Request    ${BASE_URL}${ENDPOINT}    data=${payload}    headers=${headers}    auth=${AUTH} 
    #Assert that response is empty. Empty = User Created
    Should Be Equal As Numbers    ${response.status_code}    201


Test that we cannot get the bubble location
    ${auth}=  Evaluate  ("${USERMAME}", "${PASSWORD}")
    ${params} =    Create Dictionary    username=${USERMAME}   password=${PASSWORD}
    ${response}=  GET  ${BASE_URL}/is/DEV  auth=${auth}    params=${params}    expected_status=404
    Should Contain  ${response.text}  Bubble with customer reference `${TENANT}` does not have a galaxy gateway url

    
*** Keywords ***
Post Request
    [Arguments]    ${url}    ${data}    ${headers}    ${auth}
    ${response}=    POST    ${url}    json=${data}    headers=${headers}    auth=${auth}
    RETURN    ${response}
