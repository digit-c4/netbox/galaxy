*** Settings ***
Test Tags  Docs
Library  RequestsLibrary

*** Variables ***
${BASE_URL}       http://localhost:1882
${ADMIN_TOKEN}    this1tokenshouldbeusedfortestingpurposes
${TENANT}    ISP
${ENDPOINT}      /api/${TENANT}/user
${USERMAME}    robot_new_user_test
${PASSWORD}    robot_new_user_test

*** Test Cases ***
Check Create User
    # Define Authentication
    ${auth}=  Evaluate  ("${USERMAME}", "${PASSWORD}")

    # Define the headers
    ${headers}=    Create Dictionary    accept=*/*    adminToken=${ADMIN_TOKEN}    content-type=application/json

    # Define the payload
    ${payload}=    Create Dictionary    username=${USERMAME}   password=${PASSWORD}

    # Perform the POST request
    ${response}=    Post Request    ${BASE_URL}${ENDPOINT}    data=${payload}    headers=${headers}    auth=${AUTH}
    
    #Assert that response is empty. Empty = User Created
    Should Be Equal As Numbers    ${response.status_code}    201



Check If User Already Exists
    # Define Authentication
    ${auth}=  Evaluate  ("${USERMAME}", "${PASSWORD}")

    # Define the headers
    ${headers}=    Create Dictionary    accept=*/*    adminToken=${ADMIN_TOKEN}    content-type=application/json

    # Define the payload
    ${payload}=    Create Dictionary    username=${USERMAME}   password=${PASSWORD}

    # Perform the POST request
    ${msg}=    BuiltIn.Run Keyword And Expect Error    *HTTPError: 409*    Post Request    ${BASE_URL}${ENDPOINT}    data=${payload}    headers=${headers}    auth=${AUTH}
    Should Contain    ${msg}    409
    
*** Keywords ***
Post Request
    [Arguments]    ${url}    ${data}    ${headers}    ${auth}
    ${response}=    POST    ${url}    json=${data}    headers=${headers}    auth=${auth}
    RETURN    ${response}