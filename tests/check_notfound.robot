*** Settings ***
Test Tags  Docs
Library  RequestsLibrary

*** Variables ***
${BASE_URL}       http://localhost:1882
${ADMIN_TOKEN}    this1tokenshouldbeusedfortestingpurposes
${TENANT}    ISP
${TENANT_NOTFOUND}    tenant_notfound
${ENDPOINT}      /api/${TENANT}/user
${USERMAME}    robot_notfound_test
${PASSWORD}    robot_notfound_test

*** Test Cases ***


Test User not found scenario
    ${auth}=  Evaluate  ("${USERMAME}", "${PASSWORD}")
    ${params} =    Create Dictionary    username=${USERMAME}     password=${PASSWORD} 
    ${response}=  GET  ${BASE_URL}/is/RPS  auth=${auth}    params=${params}    expected_status=404
    Should Contain  ${response.text}  User ${USERMAME} does not exist.

Create user for test
    # Define Authentication
    ${auth}=  Evaluate  ("${USERMAME}", "${PASSWORD}")
    # Define the headers
    ${headers}=    Create Dictionary    accept=*/*    adminToken=${ADMIN_TOKEN}    content-type=application/json
    # Define the payload
    ${payload}=    Create Dictionary    username=${USERMAME}   password=${PASSWORD}
    # Perform the POST request
    ${response}=    Post Request    ${BASE_URL}${ENDPOINT}    data=${payload}    headers=${headers}    auth=${AUTH}
    #Assert that response is empty. Empty = User Created
    Should Be Equal As Numbers    ${response.status_code}    201


Test tenant not found scenario
    ${auth}=  Evaluate  ("${USERMAME}", "${PASSWORD}")
    ${params} =    Create Dictionary    username=${USERMAME}     password=${PASSWORD}
    ${response}=  GET  ${BASE_URL}/is/${TENANT_NOTFOUND}  auth=${auth}    params=${params}    expected_status=404
    Should Contain  ${response.text}  IS `${TENANT_NOTFOUND}` not found

*** Keywords ***
Post Request
    [Arguments]    ${url}    ${data}    ${headers}    ${auth}
    ${response}=    POST    ${url}    json=${data}    headers=${headers}    auth=${auth}
    RETURN    ${response}