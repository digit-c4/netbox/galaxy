*** Settings ***
Test Tags  Docs
Library  RequestsLibrary

*** Variables ***
${BASE_URL}       http://localhost:1882
${ADMIN_TOKEN}    this1tokenshouldbeusedfortestingpurposes
${TENANT}    ISP
${ENDPOINT}      /api/${TENANT}/user
${USERMAME}    robot_getbubble_test
${PASSWORD}    robot_getbubble_test

*** Test Cases ***

Create user for test
    # Define Authentication
    ${auth}=  Evaluate  ("${USERMAME}", "${PASSWORD}")
    # Define the headers
    ${headers}=    Create Dictionary    accept=*/*    adminToken=${ADMIN_TOKEN}    content-type=application/json
    # Define the payload
    ${payload}=    Create Dictionary    username=${USERMAME}   password=${PASSWORD}
    # Perform the POST request
    ${response}=    Post Request    ${BASE_URL}${ENDPOINT}    data=${payload}    headers=${headers}    auth=${AUTH} 
    #Assert that response is empty. Empty = User Created
    Should Be Equal As Numbers    ${response.status_code}    201


Test that can get the bubble location
    # Define Authentication
    ${auth}=  Evaluate  ("${USERMAME}", "${PASSWORD}")
    # Perform the GET request
    ${response}=  GET  ${BASE_URL}/is/${TENANT}  auth=${auth}    expected_status=200


Test that we cannot get the bubble location
    ${auth}=  Evaluate  ("${USERMAME}", "${PASSWORD}")
    ${params} =    Create Dictionary    username=${USERMAME}   password=${PASSWORD}
    ${response}=  GET  ${BASE_URL}/is/RPS  auth=${auth}    params=${params}    expected_status=401
    Should Contain  ${response.text}  User${USERMAME} is not authorized to access this resource.

    
*** Keywords ***
Post Request
    [Arguments]    ${url}    ${data}    ${headers}    ${auth}
    ${response}=    POST    ${url}    json=${data}    headers=${headers}    auth=${auth}
    RETURN    ${response}
