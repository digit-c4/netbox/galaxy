*** Settings ***
Test Tags  Docs
Library  RequestsLibrary

*** Variables ***

${BASE_URL}  http://localhost:1882
${USER_API}  admin
${PASSWORD_API}  password

*** Test Cases ***

Test that we can get the Docs Page
    ${auth}=  Evaluate  ("${USER_API}", "${PASSWORD_API}")
    ${response}=  GET  ${BASE_URL}/docs  auth=${auth}  expected_status=200
    Should Contain  ${response.text}  Console — C4 Gateway Galaxy
