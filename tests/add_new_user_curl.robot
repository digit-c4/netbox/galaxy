*** Settings ***
Test Tags  Docs
Library  RequestsLibrary
LIbrary  Process

*** Variables ***

${BASE_URL}  http://localhost:1882
${USER_API}  admin
${PASSWORD_API}  password
${ADMIN_TOKEN}    this1tokenshouldbeusedfortestingpurposes

*** Test Cases ***

Test add_user using curl.
    ${tenant}=  Set Variable    ISP
    ${header_token}=  Set Variable    -H "adminToken: ${ADMIN_TOKEN}"
    ${body}=  Set Variable    --data '{"username": "robot_new_user_curl_test", "password": "test_robot_pass"}'
    ${header}=  Set Variable    -H "Content-Type: application/json" -H "accept: */*"
    ${user}=  Set Variable    --user ${USER_API}:${PASSWORD_API}
    ${url}=  Set Variable    ${BASE_URL}/api/${tenant}/user

    # first line with tag version
    ${res0}=    Run Process    curl -X POST ${url} ${user} ${header} ${header_token} ${body}    shell=true

    Log to console    curl -X POST ${url} ${user} ${header} ${header_token} ${body}

    Should Contain    ${res0.stdout}  created

