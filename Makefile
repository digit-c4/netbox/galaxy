.PHONY: docs
docs:
	@rm -rf docs/build
	@$(MAKE) -C docs html
